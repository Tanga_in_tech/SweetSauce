
var passport = require('passport');
var User = require('../models/User');
var Test = require('../models/Test');
var Command = require('../models/Command');
var moment = require('moment');

/**
* GET /
* Home page.
*/
exports.index = function(req, res) {
  if(!req.user){
    res.status(404)
      .send('Not Found');
  }

  Test.find({
    user:req.user.id
  },function(err, tests){
    res.render('test/index',{
      tests:tests,
      moment:moment
    });
  });
};

exports.getTest = function(req, res) {
  if(!req.user) {
    res.status(404)
      .send('Not Found');
    return;
  }

  Test.findOne({
    sessionId:req.params.session_id
  }, 
  function(err, test){
    if(err) {
      res.status(404)
        .send('Not Found');
      return;
    }
    Command.find({test:test},
      function(err, commands){
        if(err){
          res.status(404)
            send('Not Found')
        }
        console.log(commands.length);
        res.render('test/test',{
          commands:commands,
          test:test,
          moment:moment
        });
      });
  });
}