var mongoose = require('mongoose');

var testSchema = new mongoose.Schema({
	name:String,
	authenticated:Boolean,
	sessionId:{ type:String, index:true },
	browserName:String,
	version:String,
	platform:String,
	passed:Boolean,
	startTime:Date,
	endTime:Date,
	createdAt:Date,
	platform:String,
	visibility:String,
	runTime:Number,
	user:{ type : mongoose.Schema.ObjectId, ref : 'User' },
	commands:[{ type : mongoose.Schema.ObjectId, ref : 'Test' }]
});



module.exports = mongoose.model('Test', testSchema);